import json
import os
import pytest

from level1 import generate_output_file, get_article_price, sum_cart


def test_data_exists():
    assert os.path.exists('data.json')


def test_data_is_a_file():
    assert os.path.isfile('data.json')


def test_output_exists():
    assert os.path.exists('output.json')


def test_output_is_a_file():
    assert os.path.isfile('output.json')


@pytest.mark.parametrize(
    'id, result',
    [
        (1, 100),
        (2, 200),
        (3, 400),
        (4, 1000),
    ]
)
def test_get_price_article_id(id, result):
    data_file = open('data.json', 'r')
    data_json = json.load(data_file)
    data_file.close()
    assert result == get_article_price(id, data_json)


def test_sum_cart_id_1():
    data_file = open('data.json', 'r')
    data_json = json.load(data_file)
    data_file.close()
    assert 2000 == sum_cart(1, data_json)


def test_sum_cart_id_2():
    data_file = open('data.json', 'r')
    data_json = json.load(data_file)
    data_file.close()
    assert 1400 == sum_cart(2, data_json)


def test_sum_cart_id_3():
    data_file = open('data.json', 'r')
    data_json = json.load(data_file)
    data_file.close()
    assert 0 == sum_cart(3, data_json)


def test_generated_file_is_equal_output():
    data_file = open('data.json', 'r')
    output_file = open('output.json', 'r')

    data_json = json.load(data_file)
    data_file.close()

    output_json = json.load(output_file)
    output_file.close()
    assert output_json == generate_output_file(data_json)
