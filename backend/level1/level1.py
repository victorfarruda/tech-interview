"""
{
  "articles": [
    { "id": 1, "name": "water", "price": 100 },
    { "id": 2, "name": "honey", "price": 200 },
    { "id": 3, "name": "mango", "price": 400 },
    { "id": 4, "name": "tea", "price": 1000 }
  ],
  "carts": [
    {
      "id": 1,
      "items": [
        { "article_id": 1, "quantity": 6 },
        { "article_id": 2, "quantity": 2 },
        { "article_id": 4, "quantity": 1 }
      ]
    },
    {
      "id": 2,
      "items": [
        { "article_id": 2, "quantity": 1 },
        { "article_id": 3, "quantity": 3 }
      ]
    },
    {
      "id": 3,
      "items": []
    }
  ]
}
"""
import json


def get_article_price(article_id, file):
    for article in file['articles']:
        if article['id'] == article_id:
            return article['price']


def sum_cart(id, file):
    sum_value = 0
    for cart in file['carts']:
        if cart['id'] == id:
            for item in cart['items']:
                sum_value += get_article_price(item['article_id'], file) * item['quantity']
    return sum_value


def generate_output_file(file):
    carts = {}
    carts['carts'] = []
    for cart in file['carts']:
        sum_selected = sum_cart(cart['id'], file)
        new_cart = {}
        new_cart['id'] = cart['id']
        new_cart['total'] = sum_selected
        carts['carts'].append(new_cart)
    with open('new_output.json', 'w') as new_file:
        json.dump(carts, new_file, indent=2)
        new_file.close()
    json_dump = json.dumps(carts)
    return json.loads(json_dump)



"""
{
  "carts": [
    {
      "id": 1,
      "total": 2000
    },
    {
      "id": 2,
      "total": 1400
    },
    {
      "id": 3,
      "total": 0
    }
  ]
}
"""