import json
import os
import pytest

from level3 import (
generate_output_file,
    get_article_price,
    sum_cart,
    sum_cart_with_delivery
)


def test_data_exists():
    assert os.path.exists('data.json')


def test_data_is_a_file():
    assert os.path.isfile('data.json')


def test_output_exists():
    assert os.path.exists('output.json')


def test_output_is_a_file():
    assert os.path.isfile('output.json')


@pytest.mark.parametrize(
    'id, result',
    [
        (1, 100),
        (2, 175),
        (3, 400),
        (4, 1000),
    ]
)
def test_get_price_article_id(id, result):
    data_file = open('data.json', 'r')
    data_json = json.load(data_file)
    data_file.close()
    assert result == get_article_price(id, data_json)


@pytest.mark.parametrize(
    'cart_id, cart_total',
    [
        (1, 1950),
        (2, 1375),
        (3, 1398),
    ]
)
def test_sum_cart(cart_id, cart_total):
    data_file = open('data.json', 'r')
    data_json = json.load(data_file)
    data_file.close()
    assert cart_total == sum_cart(cart_id, data_json)


@pytest.mark.parametrize(
    'cart_id, cart_total',
    [
        (1, 2350),
        (2, 1775),
        (3, 1798),
        (4, 1083),
        (5, 1196),
    ]
)
def test_sum_cart_with_delivery(cart_id, cart_total):
    data_file = open('data.json', 'r')
    data_json = json.load(data_file)
    data_file.close()
    assert cart_total == sum_cart_with_delivery(cart_id, data_json)


def test_generated_file_is_equal_output():
    data_file = open('data.json', 'r')
    output_file = open('output.json', 'r')

    data_json = json.load(data_file)
    data_file.close()

    output_json = json.load(output_file)
    output_file.close()
    assert output_json == generate_output_file(data_json)