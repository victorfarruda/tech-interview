"""
{
  "articles": [
    { "id": 1, "name": "water", "price": 100 },
    { "id": 2, "name": "honey", "price": 200 },
    { "id": 3, "name": "mango", "price": 400 },
    { "id": 4, "name": "tea", "price": 1000 }
  ],
  "carts": [
    {
      "id": 1,
      "items": [
        { "article_id": 1, "quantity": 6 },
        { "article_id": 2, "quantity": 2 },
        { "article_id": 4, "quantity": 1 }
      ]
    },
    {
      "id": 2,
      "items": [
        { "article_id": 2, "quantity": 1 },
        { "article_id": 3, "quantity": 3 }
      ]
    },
    {
      "id": 3,
      "items": []
    }
  ],
  "delivery_fees": [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 1000
      },
      "price": 800
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1000,
        "max_price": 2000
      },
      "price": 400
    },
    {
      "eligible_transaction_volume": {
        "min_price": 2000,
        "max_price": null
      },
      "price": 0
    }
  ]
}
"""
import json


def get_article_price(article_id, file):
    for article in file['articles']:
        if article['id'] == article_id:
            return article['price']


def get_value_delivery(sum_value, file):
    for delivery in file['delivery_fees']:
        if delivery['eligible_transaction_volume']['max_price'] is None:
            return delivery['price']
        elif delivery['eligible_transaction_volume']['min_price'] <= sum_value < delivery['eligible_transaction_volume']['max_price']:
            return delivery['price']


def sum_cart(cart_id, file):
    sum_value = 0
    for cart in file['carts']:
        if cart['id'] == cart_id:
            for item in cart['items']:
                sum_value += get_article_price(item['article_id'], file) * item['quantity']
    return sum_value


def sum_cart_with_delivery(cart_id, file):
    sum_value = sum_cart(cart_id, file)
    total = get_value_delivery(sum_value, file) + sum_value
    return total


def generate_output_file(file):
    carts = {}
    carts['carts'] = []
    for cart in file['carts']:
        sum_selected = sum_cart_with_delivery(cart['id'], file)
        new_cart = {}
        new_cart['id'] = cart['id']
        new_cart['total'] = sum_selected
        carts['carts'].append(new_cart)
    with open('new_output.json', 'w') as new_file:
        json.dump(carts, new_file, indent=2)
        new_file.close()
    json_dump = json.dumps(carts)
    return json.loads(json_dump)